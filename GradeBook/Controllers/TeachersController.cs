using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using GradeBook.Models.NHibernate;
using GradeBook.Models.NHibernate.Persistent;

namespace GradeBook.Controllers
{
    using TeachersRepository = CrudRepository<Teacher,ulong>;
    [EnableCors(origins:"*",headers:"*",methods:"*")]
    public class TeachersController : ApiController
    {
        private readonly TeachersRepository _teachersRepository = new TeachersRepository();
        public Teacher[] Get()
        {
            return _teachersRepository.GetAll().ToArray();
        }

        public Teacher[] Post(Teacher[] teachers)
        {
            for (var index = 0; index < teachers.Length; index++)
            {
                var el = teachers[index];
//                if (el.Id != 0)
//                {
//                    TeachersRepository.Update(el);
//                }
//                else
//                {
//                    TeachersRepository.Add(el);
//                }
            }
            return teachers;
        }
    }
}