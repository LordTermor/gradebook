using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using GradeBook.Models.NHibernate;
using GradeBook.Models.NHibernate.Persistent;

namespace GradeBook.Controllers
{
    using StudentRepository = CrudRepository<Student,ulong>;
    [EnableCors(origins:"*",headers:"*",methods:"*")]
    public class StudentsController:ApiController
    {
        private readonly StudentRepository _studentRepository = new StudentRepository();

        public Student[] Get()
        {
            return _studentRepository.GetAll().ToArray();
        }

        public void Post(Student st)
        {
            _studentRepository.Add(st);
        }
    }
}