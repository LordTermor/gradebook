using System;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using GradeBook.Models;
using GradeBook.Models.NHibernate;
using GradeBook.Models.NHibernate.Persistent;

namespace GradeBook.Controllers
{
    using SyllabusRepository = CrudRepository<Syllabus,ulong>;
    [EnableCors(origins:"*",headers:"*",methods:"*")]
    public class SyllabusController : ApiController
    {
        private readonly SyllabusRepositoryProxy _syllabusRepository = new SyllabusRepositoryProxy(null);
        private readonly CrudRepository<User, ulong> _userRepository = new CrudRepository<User, ulong>();

        public Syllabus[] Get()
        {
            string token = HttpContext.Current.Request.QueryString["token"];

            _syllabusRepository.Token = token;
            var arr = _syllabusRepository.GetAll().ToArray();
            return arr;
        }

        public Syllabus[] Post(Syllabus[] items)
        {
            string token = HttpContext.Current.Request.QueryString["token"];

            _syllabusRepository.Token = token;
            foreach (var el in items)
            {
                _syllabusRepository.Update(el);
            }

            return _syllabusRepository.GetAll().ToArray();
        }
    }
}