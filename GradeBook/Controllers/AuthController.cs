using System.Linq;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using GradeBook.Models.NHibernate;
using GradeBook.Models.NHibernate.Persistent;

namespace GradeBook.Controllers
{
    using TokenRepository = CrudRepository<Token,string>;
    using CredentialsRepository = CrudRepository<Credentials,string>;
    
    [EnableCors(origins:"*",headers:"*",methods:"*")]
    public class AuthController:ApiController
    {
        private readonly TokenRepository _tokenRepository = new TokenRepository();
        private readonly CredentialsRepository _credentialsRepository = new CredentialsRepository();
        public Credentials Get(string login,string pwd)
        {
            var creds = _credentialsRepository.Get(login);
            if (pwd == creds?.Password)
            {
                if (creds == null)
                {
                    return null;
                }
                
                if (creds.ActualToken != null)
                {
                    return creds;
                }
                else
                {
                    var token = Token.Generate(creds);
                    _tokenRepository.Add(token);
                    creds.ActualToken = token;
                    _credentialsRepository.Update(creds);
                    return creds;
                }
            }

            return null;
        }
    }
}