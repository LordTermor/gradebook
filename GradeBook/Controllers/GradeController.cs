using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Cors;
using GradeBook.Models;
using GradeBook.Models.NHibernate;
using GradeBook.Models.NHibernate.Persistent;
using Newtonsoft.Json;

namespace GradeBook.Controllers
{
    using GradeRepository = CrudRepository<Grade,ulong>;
    
    [EnableCors(origins:"*",headers:"*",methods:"*")]
    public class GradeController:ApiController
    {
        GradeBookRepositoryProxy _gradeRepository = new GradeBookRepositoryProxy(null);

        public Grade[] Get()
        {
            string token = HttpContext.Current.Request.QueryString["token"];

            _gradeRepository.Token = token;
            return _gradeRepository.GetAll().ToArray();        
        }
        public Grade[] Post()
        {
            string token = HttpContext.Current.Request.QueryString["token"];

            _gradeRepository.Token = token;
            
            var bodyStream = new StreamReader(HttpContext.Current.Request.InputStream);
            bodyStream.BaseStream.Seek(0, SeekOrigin.Begin);
            var bodyText = bodyStream.ReadToEnd();
            
            var grades = JsonConvert.DeserializeObject<Grade[]>(bodyText);
            
            foreach (var grade in grades) _gradeRepository.Update(grade);
            
            return _gradeRepository.GetAll().ToArray();        
        }
    }
}