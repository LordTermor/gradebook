import {ICredentials, IStudent, ISubject, ISyllabusItem, ITeacher, IUser} from "../@types";
import {sha256} from "js-sha256";
import {ReactCookieProps} from "react-cookie";
const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/;

function reviver(key:any, value:any) {
    if (dateFormat.test(value)) {
        return new Date(value);
    }

    return value;
}
export default class QueryService<TEntity> {
    
    token:string;
    entityName:string;
    constructor(entityName:string,token:string){
        this.entityName = entityName;
        this.token=token;
    }
private static get(endpoint:string):Promise<any>{
    return fetch(endpoint).then((data) => {
        return data.json();
    });
}
private static post(endpoint:string,data:any):Promise<any>{
    return fetch(endpoint,{
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    }).then((data) => {
        return data.json()
    });
}
get():Promise<TEntity[]>{
        return QueryService.get(`http://localhost:5000/api/${this.entityName}?token=${this.token}`).then(data=>{
            return data as TEntity[]});
}
submit(data:TEntity[]):Promise<TEntity[]>{
        return QueryService.post(`http://localhost:5000/api/${this.entityName}?token=${this.token}`,data).then(data=>data as TEntity[]);
}
    

    static getToken(login: string, password: string): Promise<ICredentials> {
        const encodedLogin = encodeURIComponent(login);
        const encodedPwdHash = encodeURIComponent(sha256(password));
        return QueryService.get(`http://localhost:5000/api/auth?login=${encodedLogin}&pwd=${encodedPwdHash}`);
    }
}
