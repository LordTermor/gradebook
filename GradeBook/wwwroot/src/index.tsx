import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import '@devexpress/dx-react-grid-bootstrap4/dist/dx-react-grid-bootstrap4.css';
import * as  History from "history";
import {CookiesProvider} from "react-cookie";
import App from "./App";
const history = History.createBrowserHistory();
ReactDOM.render(
    <CookiesProvider>
        <link
            rel="stylesheet"
            href="https://bootswatch.com/4/yeti/bootstrap.min.css"
            crossOrigin="anonymous"
        />
        <App history={history}/>
    </CookiesProvider>
    , document.getElementById('root'));
