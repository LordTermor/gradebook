export interface IToken{
    TokenString:string;
    StartDate:Date;
}
export interface ICredentials {
    Login:string;
    User:IUser;
    ActualToken:IToken;
}
export  interface IUser {
    Id:number;
    Name:string|null;
}
export interface ISubject {
    Id:number;
    Name: string | null;
}

export interface ITeacher extends  IUser{
    Id:number;
    Name: string | null
}

export interface IGrade {
    Id:number;
    Mark:number|null;
    Syllabus:ISyllabusItem|null;
    Student:IStudent|null;
    AttestationDate:Date|string|null;
}

export interface IStudent extends IUser{
    RecordBookNumber:number;
    BirthDate: Date |string | null;
    Course: number | null;
    PhoneNumber: number | null;
    EMail: string | null;
    Group: string | null
}

export enum AttestationType {
    Exam = "Экзамен", Credit = "Зачет", Project = "Курсовой проект"
}

export interface ISyllabusItem {
    Id:number;
    Subject: ISubject | null;
    Semester: number | null;
    Teacher: ITeacher | null;
    Type: AttestationType | null;
}
