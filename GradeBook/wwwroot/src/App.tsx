import * as React from 'react';
import {Redirect, Route, Router, Switch} from "react-router-dom";

import {History} from "history"
import {ReactCookieProps, withCookies} from "react-cookie";
import {AppPage} from "./Pages/AppPage";

export interface IAppRouterProps extends ReactCookieProps{
    history: History<any>
}

class AppRouter extends React.Component<IAppRouterProps>{
   render(){
      return <Router history={this.props.history}>
           <AppPage cookies={this.props.cookies}/>
       </Router>
   }
}

export default withCookies(AppRouter);
