import * as React from "react";
import {
    Grid,
    PagingPanel, SearchPanel,
    Table,
    TableEditColumn,
    TableEditRow,
    TableHeaderRow,
    Toolbar
} from '@devexpress/dx-react-grid-bootstrap4';
import {
    Column,
    DataTypeProvider,
    DataTypeProviderProps, EditingColumnExtension,
    EditingState, IntegratedFiltering,
    IntegratedPaging,
    IntegratedSorting,
    PagingState, SearchState,
    SortingState
} from "@devexpress/dx-react-grid";
import QueryService from "../../Services/QueryService";
import ReactLoading from "react-loading";
import Button from "react-bootstrap/Button";

const DateFormatter = ({value}: DataTypeProvider.ValueFormatterProps) => {
    if(value as string){
        return (new Date(value)).toLocaleDateString();
    }
    return (value != null) ? value.toLocaleDateString() : "";
};

const DateTypeProvider = (props: DataTypeProviderProps) => (
    <DataTypeProvider
        formatterComponent={DateFormatter}
        {...props}
    />
);

export interface IState<T> {
    entities: T[]
    isBusy:Boolean;
}

export interface IProps<T> {
    service:QueryService<T>
    columns: Column[]
    editingEnabled?:boolean;
    columnExtensions?:EditingColumnExtension[];
    addingPopupFunction?:(onAdded:(v:T)=>void)=>any;
}

export default class GradeBookTable<T> extends React.Component<IProps<T>, IState<T>> {
    
    state = {
        entities: [],
        isBusy:false
    };

    constructor(p: IProps<T>, s: IState<T>) {
        super(p, s);

        this.commitChanges = this.commitChanges.bind(this);
    }

    render(): React.ReactNode {
        return (<div style={{position:"relative"}}>
               { this.state.isBusy &&
                (<div className="justify-content-md-center" style={{
                    width: "100%",
                    height: "100%",
                    display: this.state.isBusy ? "block" : "none",
                    position: "absolute",
                    zIndex: 10,
                    background: this.state.entities.length>0?'rgba(255,255,255,0.6)':"white"
                }}>
                    <div style={{
                        position: "absolute", top: "50%",
                        left: "50%",
                        transform: 'translate(-50%, -50%)'
                    }}>
                        <ReactLoading color="#0079a1" type={"bars"}/>
                    </div>
                </div>)}
                {this.props.addingPopupFunction && this.props.addingPopupFunction((el:T)=>{this.setState({...this.state,entities:[...this.state.entities,el]})})}
                {this.props.addingPopupFunction && <Button  style={{position:"absolute",top:"0.5rem",left:"0.5rem",zIndex: 1000}}>Добавить</Button>}

                <Grid columns={this.props.columns} rows={this.state.entities}>
                <PagingState
                    defaultCurrentPage={0}
                    pageSize={20}
                />
                <Toolbar />

                <IntegratedPaging/>
                <SortingState/>
                <SearchState/>
                <IntegratedFiltering />
                <EditingState
                    columnExtensions={this.props.columnExtensions}
                    onCommitChanges={(e) => this.commitChanges(e)}
                />
                <SearchPanel  messages={{searchPlaceholder:"Поиск..."}}/>

                <IntegratedSorting/>
                <Table/>
                <TableHeaderRow showSortingControls/>
                
                <DateTypeProvider
                    for={['BirthDate','AttestationDate']}
                />
                {this.props.editingEnabled &&
                (<TableEditRow/>)}
                {this.props.editingEnabled &&
                    (<TableEditColumn
                        width={200}
                    showAddCommand={this.props.addingPopupFunction==undefined}
                    showEditCommand
                    showDeleteCommand
                    messages={{
                        addCommand:"Добавить",
                        editCommand:"Изменить",
                        cancelCommand:"Отмена",
                        commitCommand:"Применить",
                        deleteCommand:"Удалить"
                    }}
                    />)}
                <PagingPanel/>
            </Grid>
            </div>
            )
    }

    componentDidMount(): void {
        this.setState({...this.state,isBusy:true});
        this.props.service.get().then((data) => {
            
            this.setState({...this.state,
                entities: data,isBusy:false
            })

        })
    }

    private commitChanges({added, changed, deleted}: any) {
        let rows: T[] = this.state.entities;
        if (added) {
            rows = [
                ...rows,
                ...added.map((row: any, index: any) => (row)),
            ];
        }
        if (changed) {
            rows = rows.map((row: T, index: number) => (changed[index] ? Object.assign({}, row, changed[index]) : row));
        }
        if (deleted) {
            const deletedSet = new Set(deleted);
            rows = rows.filter((row, index) => !deletedSet.has(index));
        }
        this.setState({...this.state,isBusy:true});
            this.props.service.submit(rows).then(data=>{
                this.setState({...this.state,
                    entities: data,isBusy:false
                })
            });
    }
}
