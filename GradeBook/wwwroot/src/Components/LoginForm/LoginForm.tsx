import {Button, Card, Form} from "react-bootstrap";
import React from "react";
import QueryService from "../../Services/QueryService";
import ReactLoading from 'react-loading';
import {ICredentials, IUser} from "../../@types";
import Alert from "react-bootstrap/Alert";

interface ILoginFormProps {
    onTokenGot: (credentials:ICredentials) => void;
}

interface IState {
    login: string;
    password: string;
    isBusy: boolean;
    error:boolean;
}

export default class LoginForm extends React.Component<ILoginFormProps, IState> {
    state = {
        isBusy: false,
        error:false,
        login: "",
        password: ""
    };
    
    handleSubmit(){
        this.setState({...this.state, isBusy: true});
        QueryService.getToken(this.state.login, this.state.password).then((data) => {
                if(data as ICredentials) {
                    this.setState({...this.state, isBusy: false,error:false});
                    this.props.onTokenGot(data);
                } else{
                    this.setState({...this.state, isBusy: false,error:true});
                }
            }
        );
    }

    render() {
        return (<div style={{
            width: "100%",
            height: "100%",
            background: 'url(https://cdn1.img.sputniknewslv.com/images/834/81/8348170.jpg) 100% 100% no-repeat',
            backgroundSize: "cover"
        }}>

            <Card className="justify-content-md-center"
                  style={{
                      width: '18rem',
                      height: 'auto',
                      position: 'absolute',
                      top: '50%',
                      transform: 'translate(-50%, -50%)',
                      left: '50%',
                  }}>
                <div className="justify-content-md-center" style={{
                    width: "100%",
                    height: "100%",
                    display: this.state.isBusy ? "block" : "none",
                    position: "absolute",
                    zIndex: 10,
                    background: 'rgba(255,255,255,0.6)'
                }}>
                    <div style={{
                        position: "absolute", top: "50%",
                        left: "50%",
                        transform: 'translate(-50%, -50%)'
                    }}>
                        <ReactLoading color="#0079a1" type={"bars"}/>
                    </div>
                </div>

                <Card.Img
                    variant="top"
                    src='https://samgtu.ru/uploads/thumbs/logo123-f770855da3-871664ab6df303814024906dace199c0.png'
                />
                <Card.Body>
                    <Form autoComplete={'on'}>
                        <Form.Group controlId="formBasicLogin">
                            <Form.Label>Логин</Form.Label>
                            <Form.Control onChange={(e: any) => {
                                this.setState({...this.state, login: e.target.value})
                            }
                            }
                                          value={this.state.login}
                                          type='text'
                                          name='username'
                                          placeholder="Введите логин"/>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Пароль</Form.Label>
                            <Form.Control onKeyPress={(e:any)=>{
                                if(e.key === "Enter"){
                                    this.handleSubmit();
                                }
                            }} onChange={(e: any) => {
                                this.setState({...this.state, password: e.target.value})
                            }}
                                          value={this.state.password}
                                          type="password"
                                          placeholder="Введите пароль"/>
                        </Form.Group>
                        <Alert  variant={'danger'} style={{display:this.state.error?"block":"none"}}>Неверный вход!</Alert>
                            <Button  variant="primary" onClick={(event: any) => {
                           this.handleSubmit();
                        }
                        }>
                            Вход
                        </Button>
                    </Form>
                </Card.Body>
            </Card>

        </div>)
    }
}
