import {Route} from "react-router";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import {LinkContainer} from "react-router-bootstrap";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import QueryService from "../../Services/QueryService";
import GradeBookTable from "../../Components/GradeBookTable/GradeBookTable";
import React from "react";
import {ICredentials, IGrade, IStudent, ISubject, ISyllabusItem, ITeacher, IUser} from "../../@types";
import {Cookies, ReactCookieProps} from "react-cookie";
import {Column, EditingColumnExtension, EditingState} from "@devexpress/dx-react-grid";
import {ButtonGroup, InputGroup, Modal, ModalDialog} from "react-bootstrap";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

const AttestationTypeNames = [
    "Экзамен", "Зачет", "Курсовой проект"
];
interface MenuItem{
    name:string;
    to:string;
    columns:Column[];
    service:QueryService<any>;
    editable:boolean;
    columnExtensions?:EditingColumnExtension[];
    addingPopupFunction?:(onAdded:(arg0:any)=>void)=>any
}

interface IState{
    availableMenuItems:MenuItem[];
}
interface IProps {
    credentials: ICredentials;
    cookies?: Cookies;
}
export default class MainPage extends React.Component<IProps, IState> {
    menuItems:MenuItem[] = [
        {
            name: "Успеваемость",
            to: "/gradebook",
            columns:[
                {title:"Предмет",name:"SubjectName",getCellValue:(row:any)=>row.Syllabus.Subject.Name},
                {title:"Студент",name:"StudentName",getCellValue:(row:any)=>row.Student.Name},
                {title:"Семестер",name:"Semester",getCellValue:(row:any)=>row.Syllabus.Semester},
                {title:"Оценка",name:"Mark"}
            ],
            columnExtensions:[
                {
                    columnName:"SubjectName",

                    createRowChange:(row:IGrade,value:string)=>((row && row.Syllabus)?{Syllabus:{...row.Syllabus,Subject:row?{...row.Syllabus.Subject,Name:value}:null}}:null)
                },
                {
                    columnName: "StudentName",
                    createRowChange: (row: IGrade, value: string) => ((row && row.Student) ? {Student:
                       { ...row.Student,
                        Name:value}
                    } : null)
                }

            ],
            service: new QueryService<IGrade>("grade",this.props.credentials.ActualToken.TokenString),
            editable: (this.props.credentials.User as ITeacher)?true:(this.props.credentials.User as IStudent)?false:true
        },
        {
            name:  "Студенты",
            to: "/students",
            columns:[
                {title: "Номер зачетной книжки", name: "RecordBookNumber"},
                {title: "ФИО", name: "Name"},
                {title: "Группа", name: "Group"},
                {title: "Дата рождения", name: "BirthDate"},
                {title: "Курс", name: "Course"},
                {title: "Номер телефона", name: "PhoneNumber"},
                {title: "Адрес электронной почты", name: "EMail"}
            ],
            service:new QueryService<IStudent>("students",this.props.credentials.ActualToken.TokenString),
            editable: (this.props.credentials.User as IStudent)?false:true
        },
        {
            name: "Дисциплины",
            to: "/subjects",
            columns:[
                {title: "Название", name: "Name"}
            ],
            service:new QueryService<ISubject>("subjects",this.props.credentials.ActualToken.TokenString),
            editable: (this.props.credentials.User as IStudent)?false:true
        },
        {
            name: "Преподаватели",
            to: "/teachers",
            columns:[
                {title: "Имя", name: "Name"}
            ],
            service: new QueryService<ITeacher>("teachers",this.props.credentials.ActualToken.TokenString),
            editable: (this.props.credentials.User as IStudent)?false:true,
            addingPopupFunction:onAdded => (<ModalDialog style={{padding:20,zIndex:100,position:"fixed",left:0,top:0,right:0,bottom:0}}><Form autoComplete={'on'}>
                <Form.Group style={{padding:5}}>
                    <InputGroup><Form.Control style={{marginTop:5}} placeholder="Логин"></Form.Control><InputGroup.Append><InputGroup.Text style={{marginTop:5}}>Латинские буквы и цифры</InputGroup.Text></InputGroup.Append></InputGroup>
                    <Form.Control style={{marginTop:5}} placeholder="Имя"></Form.Control>
                    <Form.Control style={{marginTop:5}} placeholder="Пароль" type="password"></Form.Control>
                </Form.Group>
                <ButtonGroup style={{padding:5}} className="float-right"><Button style={{right:10}}  variant="primary">Добавить</Button><Button>Отмена</Button></ButtonGroup>
            </Form></ModalDialog>)
        },
        {
            name: "Учебный план",
            to: "/syllabus",
            columns:[

                {title: "Предмет", name: "SubjectName", getCellValue: (row:any) => row.Subject ? row.Subject.Name : ""},
                {
                    title: "Преподаватель",
                    name: "TeacherName",
                    getCellValue: (row:any) => row.Teacher ? row.Teacher.Name : ""
                },
                {title: "Семестр", name: "Semester"},
                {title: "Тип аттестации", name: "Type", getCellValue: (row:any) => AttestationTypeNames[row.Type]}
            ],
            service: new QueryService<ISyllabusItem>("syllabus",this.props.credentials.ActualToken.TokenString),
            editable: (this.props.credentials.User as IStudent)?false:true
        }
    ];
    state={
        availableMenuItems:[]
    };
    render() {
        return (<div>
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand>Электронный журнал</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        {this.state.availableMenuItems.map((value:any)=>{
                            return <LinkContainer to={value.to}>
                                <Nav.Link>{value.name}</Nav.Link>
                            </LinkContainer>
                        })}
                    </Nav>
                    <Navbar.Collapse className="justify-content-end">
                        <Navbar.Text>
                            <Col>
                                <Row>Здравствуйте, {this.props.credentials.User?this.props.credentials.User.Name:"Администратор"}</Row>
                                <Row className="justify-content-end">
                                    <a style={{cursor: "pointer"}} onClick={(event: any) => {
                                        // @ts-ignore
                                        this.props.cookies.remove('credentials');
                                        window.location.reload();
                                    }}>Выход</a>
                                </Row>
                            </Col>
                        </Navbar.Text>
                    </Navbar.Collapse>
                </Navbar.Collapse>
            </Navbar>
            {this.state.availableMenuItems.map((value:any)=>{
            return <Route path={value.to} render={(props) => {
                return <GradeBookTable addingPopupFunction={value.addingPopupFunction} columnExtensions={value.columnExtensions} editingEnabled={value.editable}  {...props} columns={value.columns}
                 service={value.service}/>
                    }}/>})}
        </div>)
    };
    componentWillMount(): void {
        if(this.props.credentials.User !=null){
            this.setState({
                ...this.state,
                availableMenuItems:[this.menuItems[0],this.menuItems[4]]
            })
        } else {
            this.setState({
                ...this.state,
                availableMenuItems:this.menuItems
            })
        }
        
    }
}
