import {Cookies, ReactCookieProps} from "react-cookie";
import * as React from "react";
import {Redirect, Route, Router, Switch} from "react-router";
import {ICredentials} from "../@types";
import LoginForm from "../Components/LoginForm/LoginForm";
import MainPage from "./MainPage/Main";

const sleep = (milliseconds: number) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
};


interface IState {
    credentials:ICredentials|null;
}

interface IProps extends ReactCookieProps {
    cookies?:Cookies;
}

export class AppPage extends React.Component<IProps, IState> {
    state = {credentials:null};

    render() {
        return (
            <div style={{width: "100%", height: "100%"}}>
                <Switch>
                    <Route
                        exact path={"/login"}
                        render={props => {
                            if (this.state.credentials != null) {
                                return <Redirect to={"/"}/>
                            }
                            return (
                                <LoginForm onTokenGot={
                                    (e: ICredentials) => {
                                        sleep(2000).then(() => {
                                            // @ts-ignore
                                            this.props.cookies.set('credentials', e, {path: '/'});
                                            this.setState({...this.state, credentials:e});
                                        })
                                    }
                                } {...props} />
                            )
                        }}
                    />
                    <Route path="/" render={(props) => {
                        let creds:ICredentials|null = this.state.credentials;
                        if (creds != null && (creds as ICredentials).ActualToken!=null) {
                            return <MainPage credentials={creds}
                                             cookies={this.props.cookies} {...props}/>
                        } else {
                            return <Redirect to="/login"/>
                        }
                    }
                    }/>
                </Switch>
            </div>)
    }

    componentDidMount(): void {
        
        // @ts-ignore
        this.setState({...this.state, credentials: this.props.cookies.get('credentials')})
    }
}