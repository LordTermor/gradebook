using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using FluentNHibernate.Conventions;
using GradeBook.Models.NHibernate;
using GradeBook.Models.NHibernate.Persistent;
using NHibernate;
using NHibernate.Criterion;

namespace GradeBook.Models
{
    public class GradeBookRepositoryProxy: CrudRepository<Grade, ulong>
    {
        private readonly CrudRepository<User,ulong> _userRepository = new CrudRepository<User, ulong>();
        private readonly CrudRepository<Grade,ulong> _gradeRepository = new CrudRepository<Grade, ulong>();
        private User _user;
        public GradeBookRepositoryProxy(string token)
        {
          
        }

        public string Token
        {
            get { return _user.Credentials[0].ActualToken.TokenString; }
            set
            {

                User user = null;
                Credentials creds = null;
                Token tkn = null;
                _user = NHibernateHelper.OpenSession().QueryOver<User>(() => user)
                    .JoinQueryOver(user1 => user1.Credentials, () => creds)
                    .JoinQueryOver(credentials => credentials.ActualToken, () => tkn)
                    .Where(token1 => token1.TokenString == value).SingleOrDefault();
            }
        }

        public override ulong Add(Grade pesist)
        {
            return base.Add(pesist);
        }

        public override IQueryable<Grade> GetAll()
        {
            if (_user is Student)
            {
                return base.GetAll().Where(x=>x.Student==_user);  
            }

            if (_user is Teacher)
            {
                return base.GetAll().Where(x => x.Syllabus.Teacher == _user);
            }
            else return base.GetAll();
        }

        public override void Update(Grade persist)
        {
            if (_user is Student)
            {
                return;
            }
            if (_user is Teacher && persist.Syllabus.Teacher==_user)
            {
                base.Update(persist);
                return;
            } 
            base.Update(persist);
        }

        public override void Delete(ulong key)
        {
            base.Delete(key);
        }

        public override Grade Get(ulong key)
        {
            return base.Get(key);
        }
    }
}