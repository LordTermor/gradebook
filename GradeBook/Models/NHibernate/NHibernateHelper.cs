﻿using System;
using System.Configuration;
using FluentNHibernate.Cfg;
using GradeBook.Models.NHibernate.Persistent;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace GradeBook.Models.NHibernate
{
    public static class NHibernateHelper
    {
        public static ISession OpenSession()
        {
            ISessionFactory sessionFactory = Fluently.Configure()
                .Database(FluentNHibernate.Cfg.Db.MySQLConfiguration.Standard
                .ConnectionString("Server=127.0.0.1;Database=gradebook;Uid=root").ShowSql()
            )
                .Mappings(m =>m.FluentMappings.AddFromAssemblyOf<MvcApplication>())
            .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(true, true))
            .BuildSessionFactory();

            return sessionFactory.OpenSession();
        }
    }
}
