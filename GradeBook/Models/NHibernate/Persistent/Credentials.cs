using System.Reflection;
using FluentNHibernate.Mapping;

namespace GradeBook.Models.NHibernate.Persistent
{
    public class Credentials
    {

        public virtual string Login { get; set; }
        protected internal virtual string Password { get; set; }

        public virtual Token ActualToken { get; set; }
        
        public virtual User User { get; set; }


        public class Map : ClassMap<Credentials>
        {
            public Map()
            {
                Id(x => x.Login);
                Map(x => x.Password);
                References(x => x.User).Unique().LazyLoad(Laziness.False);
                References(x => x.ActualToken).Unique().LazyLoad(Laziness.False);
            }
        }
    }
}