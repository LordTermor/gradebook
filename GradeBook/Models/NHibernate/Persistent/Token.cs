using System;
using System.Collections.Generic;
using FluentNHibernate.Mapping;

namespace GradeBook.Models.NHibernate.Persistent
{
    public class Token
    {
        
        protected internal Token(){}
        public virtual string TokenString { get; set; }
        public virtual DateTime StartDate { get; set; }
        
        public virtual IList<Credentials> Credentials { get; set; }

        public class Map : ClassMap<Token>
        {
            public Map()
            {
                Id(x => x.TokenString);
                Map(x => x.StartDate);

                HasMany<Persistent.Credentials>(x => x.Credentials).Not.KeyUpdate().Not.LazyLoad();
            }
        }

        public static Token Generate(Credentials credentials)
        {
            var token = new Token()
            {
            
                TokenString = KeyGenerator.GetUniqueKey(45),
                
                StartDate = DateTime.Now
            };
            credentials.ActualToken = token;
            return token;
        }
    }
}