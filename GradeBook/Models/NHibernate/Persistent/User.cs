using System.Collections.Generic;
using FluentNHibernate.Mapping;

namespace GradeBook.Models.NHibernate.Persistent
{
    public abstract class User
    {
        public virtual ulong Id { get; set; }
        public virtual string Name { get; set; }
        
        public virtual IList<Credentials> Credentials  { get; set; }
      
    }
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);

            HasMany(x => x.Credentials).Not.KeyUpdate().Not.LazyLoad();

        }
    }    
    
}