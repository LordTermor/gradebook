using System;
using FluentNHibernate.Mapping;

namespace GradeBook.Models.NHibernate.Persistent
{
    public class Grade
    {
        public virtual ulong Id { get; set; }
        public virtual Syllabus Syllabus { get; set; }
        public virtual Student Student { get; set; }
        public virtual DateTime AttestationDate { get; set; }
        public virtual ushort? Mark { get; set; }

        public class Map : ClassMap<Grade>
        {
            public Map()
            {
                Id(x => x.Id);
                References(x => x.Syllabus).UniqueKey("StudentKey").LazyLoad(Laziness.False);
                References(x => x.Student).UniqueKey("StudentKey").LazyLoad(Laziness.False);
                Map(x => x.AttestationDate).UniqueKey("StudentKey");
                Map(x => x.Mark).Check("Mark BETWEEN 2 and 5");
            }
        }
    }
}