using FluentNHibernate.Mapping;

namespace GradeBook.Models.NHibernate.Persistent
{
    public enum AttestationType
    {
        Exam,Credit,Project
    }
   
    public class Syllabus
    {
        public virtual ulong Id { get; set; }
        public  virtual Subject Subject { get; set; }
        public  virtual ushort? Semester { get; set; }
        public  virtual Teacher Teacher { get; set; }
        public virtual AttestationType? Type { get; set; }

    }

    public class Map : ClassMap<Syllabus>
    {
        public Map()
        {
            Id(x => x.Id);
            References(x => x.Subject).LazyLoad(Laziness.False).Cascade.All();
            References(x => x.Teacher).LazyLoad(Laziness.False).Cascade.All();
            Map(x => x.Semester).Check("Semester between 1 and 8");
            Map(x => x.Type).CustomType<GenericEnumMapper<AttestationType>>();
        }
    }
}