using System.Collections.Generic;
using FluentNHibernate.Mapping;
using NHibernate.Mapping;

namespace GradeBook.Models.NHibernate.Persistent
{
    public class Subject
    {
        public virtual ulong Id { get; set; }
        public virtual string Name { get; set; }
        
        
        public virtual IList<Grade> Grades { get; set; } 

        public class Map : ClassMap<Subject>
        {
            public Map()
            {
                Id(x => x.Id);
                Map(x => x.Name);
            }
        }
    }
}