using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Web.UI.WebControls;
using FluentNHibernate.Mapping;

namespace GradeBook.Models.NHibernate.Persistent
{
    
    public class Student:User
    {
        public virtual ulong RecordBookNumber { get; set; }
        public virtual DateTime? BirthDate { get; set; }
        public virtual ushort? Course { get; set; }
        
        public virtual IList<Grade> Grades { get; set; }
        
        public virtual ulong? PhoneNumber { get; set; }
        public virtual string EMail { get; set; }
        
        public virtual string Group { get; set; }
        
       
    }
    public class StudentMap : SubclassMap<Student>
    {
        public StudentMap()
        {
            Map(x => x.RecordBookNumber).Unique();
            Map(x => x.Course).Check("Course BETWEEN 1 and 4");
            Map(x => x.BirthDate);
            Map(x => x.PhoneNumber);
            Map(x => x.EMail);
            Map(x=>x.Group).Column("EducationalGroup");
        }
    }
  
}