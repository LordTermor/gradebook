﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Transform;

namespace GradeBook.Models.NHibernate
{
    public class CrudRepository<T,TKeyType> 
    {     

        public virtual TKeyType Add(T pesist)
        {          
            using (ISession s = NHibernateHelper.OpenSession())
            using (ITransaction tr = s.BeginTransaction()) 
            {
                var obj = s.Save(pesist);
                tr.Commit();
                return (TKeyType)obj;
            }
        }

        public virtual IQueryable<T> GetAll()
        {
                return NHibernateHelper.OpenSession().Query<T>();
        }

        public virtual void Update(T persist)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Update(persist);
                transaction.Commit();
            }
        }

        public virtual void Delete(TKeyType key)
        {
            using (ISession s = NHibernateHelper.OpenSession())
            using (ITransaction tr = s.BeginTransaction())
            {
                s.Delete((Get(key)));
                tr.Commit();
            }
        }

        public virtual T Get(TKeyType key)
        {
            T notes;
            using (ISession s = NHibernateHelper.OpenSession())
            {
                notes= s.Get<T>(key);
            }

            return notes;
        }
    }
}
